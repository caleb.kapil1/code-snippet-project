#  In-app purchase
*Un-lock the question pay by IAP (In-app Purchase)*
```
override func viewDidLoad() {
    super.viewDidLoad();


    product_id = "com.ith.testprep.upgrade1";

    SKPaymentQueue.defaultQueue().addTransactionObserver(self)

    //Check if product is purchased

    if (defaults.boolForKey("purchased")){

        // Hide a view or show content depends on your requirement

        var alert = UIAlertController(title: "Series 7", message: "Already Purchased", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }

    var productID:NSSet = NSSet(object: self.product_id!);
    var productsRequest:SKProductsRequest = SKProductsRequest(productIdentifiers: productID as Set<NSObject>);
    productsRequest.delegate = self;
    productsRequest.start();
}

override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(true)

    //Prevents crash when re-opening view
    SKPaymentQueue.defaultQueue().removeTransactionObserver(self)
}

@IBAction func restoreAction(sender: AnyObject) {
    if (defaults.boolForKey("purchased")){

    // Hide a view or show content depends on your requirement

        var alert = UIAlertController(title: "Series 7", message: "Already Purchased", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    else{
    if(NSUserDefaults.standardUserDefaults().boolForKey("isSliderOpen") == false)
    {
        if Reachability.isConnectedToNetwork() {

            dispatch_async(dispatch_get_main_queue(), {

            let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.Indeterminate
            loadingNotification.color = UIColor(red: 66.0/255.0, green: 106.0/255.0, blue: 177.0/255.0, alpha: 0.6)
            })

            SKPaymentQueue.defaultQueue().restoreCompletedTransactions()
        }
        else{
        self.displayAlert("", alertMessage: Constants.GeneralAppMessage.connectionError)
        }
    }
    }
}

@IBAction func unlockAction(sender: AnyObject) {

        if (defaults.boolForKey("purchased")){

        // Hide a view or show content depends on your requirement

            var alert = UIAlertController(title: "Series 7", message: "Already Purchased", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        else{
        if(NSUserDefaults.standardUserDefaults().boolForKey("isSliderOpen") == false)
        {
            self.purchaseAction()
        }
    }
}

func purchaseAction(){
    if Reachability.isConnectedToNetwork() {

    // We check that we are allow to make the purchase.
        if (SKPaymentQueue.canMakePayments())
        {
        dispatch_async(dispatch_get_main_queue(), {

            let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
            loadingNotification.mode = MBProgressHUDMode.Indeterminate
            loadingNotification.color = UIColor(red: 66.0/255.0, green: 106.0/255.0, blue: 177.0/255.0, alpha: 0.6)
        })

        buyProduct(validProd)

        //println("Fething Products");

        }else{
        //println("can't make purchases");
        }
    }
    else {
        self.displayAlert("", alertMessage: Constants.GeneralAppMessage.connectionError)
    }
}

func buyProduct(product: SKProduct){
    //    println("Sending the Payment Request to Apple");

    //    SKPaymentQueue.defaultQueue().addTransactionObserver(self)
    var payment = SKPayment(product: product)
    SKPaymentQueue.defaultQueue().addPayment(payment);
}

// MARK: - In-App purchase Delegate method

var validProd = SKProduct()

func productsRequest (request: SKProductsRequest, didReceiveResponse response: SKProductsResponse) {

    var count : Int = response.products.count
    if (count>0) {
    var validProducts = response.products
    var validProduct: SKProduct = response.products[0] as! SKProduct
    if (validProduct.productIdentifier == self.product_id) {
    //            println(validProduct.localizedTitle)
    //            println(validProduct.localizedDescription)
    //            println(validProduct.price)

    var currencyFormatter = NSNumberFormatter()
    currencyFormatter.numberStyle = .CurrencyStyle
    currencyFormatter.locale = NSLocale.currentLocale()

    buyNowButton.titleLabel?.font = UIFont(name: "Roboto-medium", size: 22)
    buyNowButton.setTitle("\(currencyFormatter.stringFromNumber(validProduct.price)!) | BUY NOW", forState: .Normal)

    validProd = validProduct

    dispatch_async(dispatch_get_main_queue(), {
    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    })

    //            buyProduct(validProduct)
    } else {
    println(validProduct.productIdentifier)
    }

    } else {

    dispatch_async(dispatch_get_main_queue(), {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        })
    }
}

func request(request: SKRequest!, didFailWithError error: NSError!) {
    //    println("Error Fetching product information");

    dispatch_async(dispatch_get_main_queue(), {
    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    })
}

func paymentQueue(queue: SKPaymentQueue!, updatedTransactions transactions: [AnyObject]!)    {
    //    println("Received Payment Transaction Response from Apple");

        dispatch_async(dispatch_get_main_queue(), {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        })

        for transaction:AnyObject in transactions {
        if let trans:SKPaymentTransaction = transaction as? SKPaymentTransaction{

            switch trans.transactionState {
                case .Purchased:

                //                println("Product Purchased");
                SKPaymentQueue.defaultQueue().finishTransaction(transaction as! SKPaymentTransaction)
                defaults.setBool(true , forKey: "purchased")

                break;
                case .Failed:
                //                println("Purchased Failed");
                SKPaymentQueue.defaultQueue().finishTransaction(transaction as! SKPaymentTransaction)


                break;


                case .Restored:
                //                println("Already Purchased");
                SKPaymentQueue.defaultQueue().finishTransaction(transaction as! SKPaymentTransaction)
                //                SKPaymentQueue.defaultQueue().restoreCompletedTransactions()

                defaults.setBool(true , forKey: "purchased")

                default:
                break;
            }
        }
    }
}

// MARK: - In-App Restore purchase Delegate method
func paymentQueueRestoreCompletedTransactionsFinished(queue: SKPaymentQueue!) {
    println("Transactions Restored")

        dispatch_async(dispatch_get_main_queue(), {
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        })

    var purchasedItemIDS = []
    for transaction:SKPaymentTransaction in queue.transactions as! [SKPaymentTransaction] {

        if transaction.payment.productIdentifier == product_id
        {

        }
}

var alert = UIAlertView(title: "Thank You", message: "Your purchase(s) were restored.", delegate: nil, cancelButtonTitle: "OK")
alert.show()
}

func paymentQueue(queue: SKPaymentQueue!, restoreCompletedTransactionsFailedWithError error: NSError!)
{
    dispatch_async(dispatch_get_main_queue(), {
    MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
    })
}
```
