# Comman Class for HTTP Interaction
*Comman server class to Interaction with REST APIs *
```
final public class ServerDataFetch {

//MARK:- GET
class func getHTTPRequest(_ APIEndPoint: String, parameters: [String: AnyObject]? = nil, completion: @escaping (JSON) -> (), failure: @escaping (String) -> ()) {

let header = getHeaders()

Alamofire.request(self.getApiUrl() + APIEndPoint, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: header as? HTTPHeaders).responseJSON { (response: DataResponse) in

self.manageAPIResponse(response, completion: { json in
completion(json)
}, failure: { message in
failure(message as String)
})
}
}

// POST
class func postHTTPRequest(_ APIEndPoint : String, parameters : [String: AnyObject]? = nil, completion : @escaping (JSON) -> (),failure : @escaping (AnyObject) -> ()) {

let baseUrl: String = self.getApiUrl()

let header = self.getHeaders()

Alamofire.request(baseUrl + "\(APIEndPoint)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: header as? HTTPHeaders).responseJSON { (response:DataResponse) in


switch(response.result) {
case .success(_):
if response.result.value != nil{

let json:JSON = JSON(response.result.value!)
completion(json)
}
break

case .failure(_):
failure(response.result.error as AnyObject)
print(response.result.error!)
break
}
}
}

//MARK:- PUT
class func putHTTPRequest(_ APIEndPoint: String, parameters: [String: AnyObject]? = nil, completion: @escaping (JSON) -> (), failure: @escaping (String) -> ()) {

let header = getHeaders()

Alamofire.request(self.getApiUrl() + "\(APIEndPoint)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: header as? HTTPHeaders).responseJSON { (response: DataResponse) in

self.manageAPIResponse(response, completion: { json in
completion(json)
}, failure: { message in
failure(message as String)
})
}
}

//MARK:- DELETE
class func deleteHTTPRequest(_ APIEndPoint: String, parameters: [String: AnyObject]? = nil, completion: @escaping (JSON) -> (), failure: @escaping (String) -> ()) {

let header = getHeaders()

Alamofire.request(self.getApiUrl() + "\(APIEndPoint)", method: .delete, parameters: parameters, encoding: JSONEncoding.default, headers: header as? HTTPHeaders).responseJSON { (response: DataResponse) in

self.manageAPIResponse(response, completion: { json in
completion(json)
}, failure: { message in
failure(message as String)
})
}
}

//MARK:- UPLOAD MULTIPART
class func uploadHTTPRequest(_ APIEndPoint: String, parameters: [String: AnyObject]? = nil, Image: UIImage, completion: @escaping (JSON) -> (), failure: @escaping (String) -> ()) {

let header = getHeaders()

Alamofire.upload(multipartFormData:
{

(multipartFormData) in
multipartFormData.append(UIImageJPEGRepresentation(Image, 0.1)!, withName: "avatar", fileName: "file.jpeg", mimeType: "image/jpeg")
}, to: Constants.URL.baseUrl + "\(APIEndPoint)", headers: header as? HTTPHeaders)
{ (result) in
switch result {
case .success(let upload, _, _):
upload.uploadProgress(closure: { (progress) in
//Print progress
})
upload.responseJSON
{ response in
//print response.result
if response.result.value != nil {
let json: JSON = JSON(response.result.value!)
completion(json)
}

}
case .failure(_):
break
}
}
}

//MARK:- Get required headers
class func getApiUrl() -> String {
if UserDefaults.standard.bool(forKey: "isAppBeingUpdated") == true {
return Constants.URL.baseUrl
}
else {
return Constants.URL_V2.baseUrl
}
}

//MARK:- Get required headers
class func getHeaders() -> NSDictionary {
return [
"Accept": "application/json",
"Content-type": "application/json"
]
}

//MARK:- Manage API's response
class func manageAPIResponse(_ response: DataResponse<Any>, completion: @escaping (JSON) -> (), failure: @escaping (String) -> ()) {
switch(response.result) {
case .success(_):

switch response.response!.statusCode {
case 200 ... 299:

//SUCCESS
if response.result.value != nil {
let json: JSON = JSON(response.result.value!)

#if DEVELOPMENT /*App is running in debuging mode*/
print(json)
#endif

completion(json)
}

case 401:
/** SESSION TOKEN
Post notification for LOGOUT from app IF SESSION TOKEN has been expired or deleted */


let json: JSON = JSON(response.result.value!)

if json["message"].exists() == true {
failure(json["message"].stringValue)
}
else {
failure(Constants.GeneralAppMessage.apiFail)
}

break

default:

//FAILURE
if response.result.value != nil {

#if DEVELOPMENT /*App is running in debuging mode*/
print(response)
#endif

let json: JSON = JSON(response.result.value!)
var errorMessage: String?

if json["message"].exists() == true {
errorMessage = json["message"].stringValue
}
else {
errorMessage = Constants.GeneralAppMessage.apiFail
}

failure(errorMessage!)

}
}

break

case .failure(_):

#if DEVELOPMENT /*App is running in debuging mode*/
print(response)
#endif

if response.result.value != nil {

let json: JSON = JSON(response.result.value!)
var errorMessage: String?

if json["message"].exists() == true {
errorMessage = json["message"].stringValue
}
else {
errorMessage = Constants.GeneralAppMessage.apiFail
}

failure(errorMessage!)
}
else {
failure(Constants.GeneralAppMessage.connectionError)
}

break
}
}

}

```
