#  Create private channels with pusher
*Create Multiple channels for each users*
```
func initPusher(users: [JSON]) {

usersArr = users

let options = PusherClientOptions(
    authMethod: AuthMethod.authRequestBuilder(authRequestBuilder: AuthPrivateChannelRequestBuilder()),
    host: .cluster("ap2")
)

pusherPersonal = Pusher(
    key: Constants.PusherChannelInfo.PUSHER_KEY,
    options: options
)

pusherPersonal.delegate = self
pusherPersonal.connection.delegate = self
self.connectPusher()

let userId: String! = SessionManager.getCurrentUserID()

    for i in 0..< usersArr!.count {
        sortedUser = [userId, usersArr![i]["id"].stringValue].sorted() as NSArray
    
        let channel = pusherPersonal.subscribe(channelName: Constants.PusherChannelInfo.PERSONAL_CHAT_CHANNEL + "." + "\(sortedUser![0])" + "." + "\(sortedUser![1])")
        let _ = channel.bind(eventName: Constants.PusherChannelInfo.PERSONAL_CHAT_EVENT, callback: { data in
        
            if let data = data as? [String: Any] {
            let senderData = data["sender"] as? [String: Any]
            let senderId: String = "\(senderData!["id"]!)"
            let messageData = data["message"] as? [String: Any]
            
            if self.isUserOnSameUserWindow(senderId) == true || SessionManager.getCurrentUserID() == senderId {
                if SessionManager.getCurrentUserID() != senderId {
                    Util.markMessageAsRead("\(messageData!["id"]!)", forAPI: Constants.APIEndPoint.readIndividual)
                }
                // Receive the message if sender is MYSELF or sender is the same person whom I'm talking with
                self.delegate?.didReceivePersonalMessage!(receivedMessage: data)
            }
            else {
                // ELSE make that message as unread
                self.updateSeenORUnseenStatus(senderId)
                Util.updateBadgeCounter()
            }
        }
    })

    self.connectPusher()
    }
}
```
