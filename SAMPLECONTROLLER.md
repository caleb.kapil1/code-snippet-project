#  A viewcontroller class
*Create a view controller for take attendance of users*

```
class TakeAttendanceViewController: UIViewController {

//MARK:- Outlets
@IBOutlet weak var txtFldRoundTables: UITextField!

@IBOutlet weak var tblGroupMember: UITableView!

@IBOutlet weak var lblNoRecordFound: UILabel!

var pickerView: UIPickerView?

//MARK:- Varibles

fileprivate var users = [TakeAttendance]()
fileprivate var roundTables = [Principle]()

private var selectedRoundTableID = String()

//MARK:- Lifecycle
override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.
    txtFldRoundTables.delegate = self
    txtFldRoundTables.layer.borderColor = Constants.CustomColor.borderColor.cgColor
    txtFldRoundTables.layer.borderWidth = 1.0
    txtFldRoundTables.layer.cornerRadius = 1.0

    self.lblNoRecordFound.isHidden = true

    self.registerNib()
    self.getRoundTables()
}

override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
}

//MARK:- Actions
@IBAction func btnCancelTap(_ sender: Any) {
    self.dismiss(animated: true, completion: nil)
}

@IBAction func btnSaveTap(_ sender: Any) {

    var attendanceDic = [[String: Any]]()

    for user in users {
        attendanceDic.append(["user_id": user.user_id, "attendance_status": user.status])
    }

    let parameter = [
    "attendance": attendanceDic
    ] as [String: Any]

    let groupId = SessionManager.getCurrentSelectedGroupId()

    HttpServerIntraction.putHTTPRequest("\(Constants.APIEndPoint.groups)" + "/\(groupId)" + Constants.APIEndPoint.roundTable + "\(selectedRoundTableID)" + Constants.APIEndPoint.attendance, parameters: parameter as [String : AnyObject], completion: { response in

        self.dismiss(animated: true, completion: nil)

    }, failure: { failure in
        Util.showAlert(title: Constants.ValidationMessage.title, message: failure, view: self)
    })
}

func doneButtonAction() {
    if self.roundTables.count > 0 {
        let selectedIndex = pickerView?.selectedRow(inComponent: 0)

        self.txtFldRoundTables.text = self.roundTables[selectedIndex!].principle_title
        selectedRoundTableID  = self.roundTables[selectedIndex!].round_table_id

        self.getListRoundTableUsers(self.roundTables[selectedIndex!].round_table_id)
    }

    txtFldRoundTables.resignFirstResponder()
}

func presentOptionTap(_ sender: UIButton) {
    self.users[sender.tag].status = 1

    updateTable(sender.tag)
}

func absentOptionTap(_ sender: UIButton) {
    self.users[sender.tag].status = 0

    updateTable(sender.tag)
}

func excuseOptionTap(_ sender: UIButton) {
    self.users[sender.tag].status = 2

    updateTable(sender.tag)
}

//MARK:- Methods
private func registerNib() {
    self.tblGroupMember.register(UINib(nibName: "PresentMemberTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.ReuseableCellID.presentMember)
    self.tblGroupMember.register(UINib(nibName: "AttendanceHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: Constants.ReuseableCellID.attendance)
}

fileprivate func getListRoundTableUsers(_ roundTableId: String) {

    let groupId = SessionManager.getCurrentSelectedGroupId()

    Util.showMBProgressHUD(view: self.view)

    HttpServerIntraction.getHTTPRequest(Constants.APIEndPoint.getMyGroups + "/\(groupId)" + Constants.APIEndPoint.roundTable + "\(roundTableId)" + Constants.APIEndPoint.member, parameters: nil, completion: { response in

        self.users.removeAll()

        if let results = response["data"].array {

            for result in results {

            self.users.append(TakeAttendance(json: result))
            }
        }

        self.tblGroupMember.delegate = self
        self.tblGroupMember.dataSource = self
        self.tblGroupMember.reloadData()

        Util.hideMBProgressHUD(view: self.view)

    }, failure: { failure in
        Util.hideMBProgressHUD(view: self.view)
    })
}

    private func updateTable(_ index: Int) {
        self.tblGroupMember.beginUpdates()
        self.tblGroupMember.reloadRows(at: [IndexPath(row: index + 1, section: 0)], with: .automatic)
        self.tblGroupMember.endUpdates()
    }
}

extension TakeAttendanceViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        pickerView = UIPickerView()
        pickerView?.dataSource = self
        pickerView?.delegate = self

        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()

        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(self.doneButtonAction))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)

        toolBar.setItems([spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        txtFldRoundTables!.inputView = pickerView
        txtFldRoundTables.inputAccessoryView = toolBar
    }
}

extension TakeAttendanceViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count + 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = self.tblGroupMember.dequeueReusableCell(withIdentifier: Constants.ReuseableCellID.attendance) as! AttendanceHeaderTableViewCell
            return cell
        }

        let cell = self.tblGroupMember.dequeueReusableCell(withIdentifier: Constants.ReuseableCellID.presentMember) as! PresentMemberTableViewCell

        cell.lblMemberName.text = users[indexPath.row - 1].user_name

        cell.btnPresent.tag = indexPath.row - 1
        cell.btnPresent.addTarget(self, action: #selector(self.presentOptionTap(_:)), for: .touchUpInside)

        cell.btnExcused.tag = indexPath.row - 1
        cell.btnExcused.addTarget(self, action: #selector(self.excuseOptionTap(_:)), for: .touchUpInside)

        cell.btnAbsent.tag = indexPath.row - 1
        cell.btnAbsent.addTarget(self, action: #selector(self.absentOptionTap(_:)), for: .touchUpInside)

        switch users[indexPath.row - 1].status {

            case 0:

            cell.btnAbsent.setImage(UIImage(named: "cancel_red"), for: .normal)
            cell.btnPresent.setImage(UIImage(named: "ellipse"), for: .normal)
            cell.btnExcused.setImage(UIImage(named: "ellipse"), for: .normal)

            break

            case 1:

            cell.btnPresent.setImage(UIImage(named: "tick"), for: .normal)
            cell.btnAbsent.setImage(UIImage(named: "ellipse"), for: .normal)
            cell.btnExcused.setImage(UIImage(named: "ellipse"), for: .normal)
            break

            case 2:

            cell.btnExcused.setImage(UIImage(named: "cancel_yellow"), for: .normal)
            cell.btnPresent.setImage(UIImage(named: "ellipse"), for: .normal)
            cell.btnAbsent.setImage(UIImage(named: "ellipse"), for: .normal)
            break

            default:
            break
        }

    return cell
    }
}


extension TakeAttendanceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 30.0
        }
            return (Constants.ScreenSize.height * 8.8) / 100 //50.0
        }
    }


extension TakeAttendanceViewController: UIPickerViewDataSource {

    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.roundTables.count
    }
}

extension TakeAttendanceViewController: UIPickerViewDelegate {
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{

        let principleTitle = self.roundTables[row].principle_title

        return principleTitle
    }
}

```
