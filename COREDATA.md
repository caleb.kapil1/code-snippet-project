#  CoreData class for any Entity
*CoreData comman class for interact with entities*
```
class PhaseContentService {
    var context: NSManagedObjectContext

    init(context: NSManagedObjectContext) {
    self.context = context
    }

    // Creates a new PhaseCueContent
    func create(grade: String, content: String) -> PhaseCueContent {

        let newItem = NSEntityDescription.insertNewObject(forEntityName: "PhaseCueContent", into: context) as! PhaseCueContent

        newItem.grade = grade
        newItem.content = content

        return newItem
    }

    // Gets a PhaseCueContent by id
    func getById(id: NSManagedObjectID) -> PhaseCueContent? {
        return context.object(with: id) as? PhaseCueContent
    }

    // Gets all with an specified predicate.

    // Predicates examples:
    // - NSPredicate(format: "first_name == %@", "Miller Thorson")
    // - NSPredicate(format: "first_name contains %@", "Miller")
    func get(withPredicate queryPredicate: NSPredicate) -> [PhaseCueContent] {
        let fetchRequest = NSFetchRequest<PhaseCueContent>(entityName: "PhaseCueContent")

        fetchRequest.predicate = queryPredicate

        do {
        let response = try context.fetch(fetchRequest as! NSFetchRequest<NSFetchRequestResult>)
        return response as! [PhaseCueContent]

        } catch let error as NSError {
            // failure
            print(error)
            return [PhaseCueContent]()
        }
    }

    // Gets all.
    func getAll() throws -> [PhaseCueContent] {
        return get(withPredicate: NSPredicate(value:true))
    }

        // Deletes a PhaseCueContent
    func delete(id: NSManagedObjectID) {
        if let delete = getById(id: id){
            context.delete(delete)
        }
    }

    // Saves all changes
    func save() throws {
        try context.save()
    }

    func update(phaseContents: [PhaseCueContent], content: String) {
        for pc in phaseContents {
            pc.setValue(content, forKey: "content")
        }
    }
}
```
