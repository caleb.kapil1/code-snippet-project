# Socket programming for chat feature
```
- (void)applicationDidBecomeActive:(UIApplication *)application {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self connectUserWithSocket];
    });
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [self closeSocketConnection];
}

#pragma mark - SocketIO Methods

- (void)connectUserWithSocket {
    [SIOSocket
    socketWithHost:[NSString
    stringWithFormat:@"%@?user_id=%@", kBaseChatURL,
    [[NSUserDefaults standardUserDefaults]
    objectForKey:@"user_id"]]
    response:^(SIOSocket *socket) {
    self.socket = socket;

    self.socket.onConnect = ^() {
    NSLog(@"Connected");
    };

    self.socket.onDisconnect = ^() {
    NSLog(@"Disconnected");
    };

    self.socket.onError = ^(NSDictionary *dict) {
    NSLog(@"Error = %@", dict);
    };

    self.socket.onReconnectionError = ^(NSDictionary *dict) {
    NSLog(@"Error = %@", dict);
    NSLog(@"RERconnection error");
    };

    self.socket.onReconnect = ^(NSInteger i) {
    NSLog(@"reconn");
    };

    [self.socket on:@"message"
    callback:^(id data) {

    NSLog(@"message = %@", data);


    [self receiveMessage:[data objectAtIndex:0]];

    }];

    [self.socket on:@"chathistory"
    callback:^(id data) {

    for (int i = 0; i < [[data objectAtIndex:0] count];
    i++) {
    [self receiveMessage:[[data objectAtIndex:0]
    objectAtIndex:i]];
    }
    }];
    }];
}

- (void)closeSocketConnection {
    [self.socket close];
}

```
