#  AppDelegate+Extenstion class for Firebase Intregration
*This extenstion class leads to the intregration of Firebase for recieve the push notifications*
```
// MARK: - UNUserNotificationCenter Delegate methods
// [START ios_10_message_handling]

@available(iOS 10, *)
extension AppDelegate: UNUserNotificationCenterDelegate {

// Receive displayed notifications for iOS 10 devices.
func userNotificationCenter(_ center: UNUserNotificationCenter,
willPresent notification: UNNotification,
withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

    // When app IS in active mode

    self.notificationDetail = notification.request.content.userInfo

    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // Messaging.messaging().appDidReceiveMessage(userInfo)
    // Print message ID.

    print("user infor is :\(self.notificationDetail)")
    if let notificationType = self.notificationDetail[gcmMessageTYPEKey] {

    guard let notificationtype = PushNotificationType(rawValue: notificationType as! String) else {
    return
    }

    switch notificationtype {

        case .ReminderNotes:
        Util.presentMemberNotes((self.notificationDetail[APS] as? NSDictionary)!)

        case .NextStepComment:
        if let aps = self.notificationDetail[APS] as? NSDictionary {
        //self.showAlert(userInfo: aps)

        self.messageExtract(userInfo: aps, presentScreen: Constants.ScreenName.comment)
        }

        case .IndividualMessage:
        if let aps = self.notificationDetail[APS] as? NSDictionary {
        UserDefaults.standard.set(self.notificationDetail[SENDER_ID]!, forKey: Constants.NSUserDefault.INDIVIDUAL_MESSAGE_SENDERID)

        self.messageExtract(userInfo: aps, presentScreen: Constants.ScreenName.reply)
        }
        case .GroupMessage:
        if let aps = self.notificationDetail[APS] as? NSDictionary {
        self.messageExtract(userInfo: aps, presentScreen: Constants.ScreenName.group)
        }
    }

// Change this to your preferred presentation option
completionHandler([])
}

func userNotificationCenter(_ center: UNUserNotificationCenter,
didReceive response: UNNotificationResponse,
withCompletionHandler completionHandler: @escaping () -> Void) {

    // When app IS NOT in active mode

    self.notificationDetail = response.notification.request.content.userInfo
    print("response is :\(self.notificationDetail)")

    if let notificationType = self.notificationDetail[gcmMessageTYPEKey] {

    guard let notificationtype = PushNotificationType(rawValue: notificationType as! String) else {
    return
    }

    switch notificationtype {

        case .GroupMessage:
        self.messageExtract(userInfo: (self.notificationDetail[APS] as? NSDictionary)!, presentScreen: Constants.ScreenName.group)

        case .IndividualMessage:
        UserDefaults.standard.set(self.notificationDetail[SENDER_ID]!, forKey: Constants.NSUserDefault.INDIVIDUAL_MESSAGE_SENDERID)
        self.messageExtract(userInfo: (self.notificationDetail[APS] as? NSDictionary)!, presentScreen: Constants.ScreenName.reply)

        case .ReminderNotes:
        Util.presentMemberNotes((self.notificationDetail[APS] as? NSDictionary)!)

        case .NextStepComment:
        self.messageExtract(userInfo: (self.notificationDetail[APS] as? NSDictionary)!, presentScreen: Constants.ScreenName.comment)
    }

completionHandler()
}

// MARK: - MessagingDelegate Delegate methods
extension AppDelegate: MessagingDelegate {
func application(application: UIApplication,
didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
    print("token is : \(deviceToken)")
    Messaging.messaging().apnsToken = deviceToken as Data
}

// [START ios_10_data_message]
// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
// To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }

    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        //    print("Firebase registration token: \(fcmToken)")

        UserDefaults.standard.setValue(fcmToken, forKey: Constants.NSUserDefault.FCM_TOKEN)
        Util.registerFCMToken()
    }
}

```
